
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Ethan
 */
public class ZeroJudgeC634 {
    // Lookup table
    static int[] primes = new int[]{
         2,  3,  5,  7, 11, 13, 17, 19, 23, 29,
        31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
        73, 79, 83, 89, 97
    };
    
    public static void main(String[] args) {
        List<MyString> list = new ArrayList<>();
        Scanner scanner =  new Scanner(System.in);
        
        while(scanner.hasNextLine()){
            MyString ms = new MyString(scanner.nextLine());
            list.add(ms);
        }
        
        Collections.sort(list, new MyString());
        display(list);
    }
    
    static void display(List<MyString> list){
        for(MyString mS : list){
            System.out.println(mS);
        }
    }
    
    static class MyString implements Comparator<MyString>{
        String s;
        StringBuilder t;
        int n, spf;
        
        public MyString(){
            super();
        }
        
        public MyString(String ss){
            super();
            s = ss;
            t = new StringBuilder(ss);
            n = 0;
            spf = 0;
            cal();
        }
        
        public final void cal(){
            int i=0;
            
            // Separate numbers
            while(i != t.length()){
                if(t.charAt(i)>='0' && t.charAt(i)<='9'){
                    n = n*10 + (t.charAt(i) - '0');
                    t.deleteCharAt(i);
                }else{
                    i++;
                }
            }
            
            // Sum of prime factors (spf)
            int tmpN = n;
            i = 0;
            while(tmpN != 1 && i != 25){
                if(tmpN % primes[i] == 0){
                    spf += primes[i];
                    tmpN /= primes[i];
                    while(tmpN % primes[i] == 0){
                        tmpN /= primes[i];
                    }
                }
                i++;
            }
            if(tmpN!=1){
                spf += tmpN;
            }
        }

        // Arrange
        @Override
        public int compare(MyString o1, MyString o2) {
            if(o1.spf - o2.spf < 0){
                return 1;
            }else if(o1.spf - o2.spf > 0){
                return -1;
            }else{
                if(!o1.t.equals(o2.t)){
                    if(o1.t.length() - o2.t.length() > 0){
                        return 1;
                    }else if(o1.t.length() - o2.t.length() < 0){
                        return -1;
                    }else{
                        return o1.t.toString().compareTo(o2.t.toString());
                    }
                }else{
                    return -(o1.n - o2.n);
                }
            }
        }
        
        // Output information for this object
        @Override
        public String toString() {
            return s;
        }
        
    }
}

/*
測資中有若干行字串 S
S 為大小寫字母及數字的混和。
例： 
S1 = dMB1WA2rjoiy8 , 可取出文字字串 T = dMBWArjoiy , 數字 N = 128 , N 的質因數和為 2 
S2 = dMB1WA2rjoiy6 , 可取出文字字串 T = dMBWArjoiy , 數字 N = 126 , N 的質因數和為 12 (2,3,7) 
請將測資中的字串排序後輸出。
規則如下：
1)  依質因數和遞減排序。
2)  質因數和相同則對 T 做遞增排序。
3)  T 也相同則對 N 做遞減排序。
--------------------------------------------------------------------------------
輸入說明：

範例輸入：
    dMB1WA2rjoiy8
    dMB1WA2rjoiy6
--------------------------------------------------------------------------------
輸出說明：

範例輸出：
    dMB1WA2rjoiy6
    dMB1WA2rjoiy8
*/