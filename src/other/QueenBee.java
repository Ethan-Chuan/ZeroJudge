package other;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class QueenBee 
{
	public static void main(String[] args) 
	{
		int l, w, h, size2D, count, maxCount=0;
		char[] beeSpaces;
		Graph g;
		HashSet<Integer> setSpePos = new HashSet<>();
		boolean[] visited; // Mark all the vertices as not visited(By default set as false) 
		Scanner scanner = new Scanner(System.in);
		
		l = scanner.nextInt();
		w = scanner.nextInt();
		h = scanner.nextInt();
		count = l*w*h;
		beeSpaces = scanner.next().toCharArray();
		
		g = new Graph(count);
		visited = new boolean[count];
		size2D = l*w;
		for(int i=0;i<h;i++) 
		{
			for(int j=0;j<size2D;j++) 
			{
				// Current Point
				int p1 = i*size2D+j;
				// Front, Back, Left, Right, Down, Up
				int[] p2 = new int[] {p1-l, p1+l, p1-1, p1+1, p1-size2D, p1+size2D};
				
				// If current point is space, then find connection
				if(beeSpaces[p1]=='0') 
				{
					setSpePos.add(p1);
					for(int k=0;k<4;k++) 
					{
						if(k%2==0) 
						{
							if(p2[k]>=i*size2D && beeSpaces[p2[k]]=='0') g.addEdge(p1, p2[k]);
						}
						else 
						{
							if(p2[k]<(i+1)*size2D && beeSpaces[p2[k]]=='0') g.addEdge(p1, p2[k]);
						}
					}
					if(p2[4]>=0  && beeSpaces[p2[4]]=='0') g.addEdge(p1, p2[4]);
					if(p2[5]<g.getSize()  && beeSpaces[p2[5]]=='0') g.addEdge(p1, p2[5]);
				}
				else
				{
					// Never visit.
					visited[p1] = true;
				}
			}
		}
		
		/*
		g.show();
		for(int value:setSpePos) 
		{
			System.out.print(value + " ");
		}*/
		
		for(int value: setSpePos) 
		{
			count = g.BFS(value, visited, setSpePos);
			if(count >= setSpePos.size()) 
			{
				maxCount = count;
				break;
			}
			else if(count > maxCount)
			{
				maxCount = count;
			}
		}
		
		System.out.println(maxCount);
	}
	
	
	static class Graph 
	{ 
	    private int V;   // No. of vertices 
	    private LinkedList<Integer> adj[]; //Adjacency Lists 
	  
	    // Constructor 
	    Graph(int v) 
	    { 
	        V = v; 
	        adj = new LinkedList[v]; 
	        for (int i=0; i<v; ++i) 
	            adj[i] = new LinkedList(); 
	    } 
	  
	    // Function to add an edge into the graph 
	    void addEdge(int v,int w) 
	    { 
	        adj[v].add(w); 
	    }
	    
	    // Function to get graph size
	    int getSize() {
	    	return V;
	    }
	    
	    // Function to show graph
	    void show() {
	    	for(int i=0;i<getSize();i++) {
	    		System.out.print(i + " : ");
	    		for(int value:adj[i]) {
	    			System.out.print(value + " ");
	    		}
	    		System.out.println();
	    	}
	    }
	  
	    // prints BFS traversal from a given source s 
	    int BFS(int s, boolean visited[], HashSet<Integer> setSpePos) 
	    {  
	        // Create a queue for BFS 
	        LinkedList<Integer> queue = new LinkedList<Integer>(); 
	  
	        // Mark the current node as visited and enqueue it 
	        visited[s]=true; 
	        queue.add(s); 
	        
	        // Counter of queue.
	        int counter = 0;
	  
	        while (queue.size() != 0) 
	        { 
	            // Dequeue a vertex from queue and print it 
	            s = queue.poll();
	            counter++;
	            setSpePos.remove(s);
	            //System.out.print(s+" "); 
	  
	            // Get all adjacent vertices of the dequeued vertex s 
	            // If a adjacent has not been visited, then mark it 
	            // visited and enqueue it 
	            Iterator<Integer> i = adj[s].listIterator(); 
	            while (i.hasNext()) 
	            { 
	                int n = i.next(); 
	                if (!visited[n]) 
	                { 
	                    visited[n] = true; 
	                    queue.add(n); 
	                } 
	            } 
	        } 
	        return counter;
	    } 
	}
}

/*

尋找蜂后 (Queen bee )
問題敘述
科學家發現了一種新型的蜜蜂，它們的蜂巢結構為立方體，蜂巢被蜂巢壁分割為好幾個小空間，
而蜂后住在巢內最大的空間中。今日為了研究這種蜜蜂，需找到蜂巢中的最大空間，以確認蜂后
位置。蜂巢的長度為Ｌ，寬度為Ｗ，高度為Ｈ，請你寫一個程式計算蜂巢中的最大空間為多少。
輸入格式
每筆 測試資料 為二列：
 第一列有 三個正整數 L、W、H（1<=L,W,H<=135），Ｌ、Ｗ、Ｈ 代表蜂巢的長、寬、高。
 第二列共有 L*W*H 個字元，前 L*W 個字元為蜂巢的第一層，接下來 L*W 個字元為蜂巢的第二層，以此類推。每個字元可以是0或1，1代表蜂巢壁，0代表蜂巢中的空間。
以輸入範例1為例，則蜂巢結構如下圖所示，蜂后所在的空間為打V，共有七格。

V10  VV1  111
1V1  VV1  1V1
010  111  111

輸出 格式
對每筆資料請 輸出 一列， 請輸出 蜂巢中的最大空間。
輸入範例 1
3 3
010101010001001111111101111
輸出範例 1
7
輸入範例 2
1 2 4
10000100
輸出範例 2
6

評分說明
此題目測資分成三組，每測資有多筆測試資料，需答對該組所有測試資料才能獲得該組分數。
各組詳細限制如下第一組 (10 分) : 1<=L、W、H<=5。
第二組 (30 分) : 1<=L、W、H<=30。
第三組 (60 分) : 1<=L、W、H<=135。

*/
