
import java.util.Scanner;

/**
 *
 * @author Ethan
 */
public class ZeroJudgeA229 {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder sb = new StringBuilder();
        while(scanner.hasNext()){
            int n = scanner.nextInt();
            Ans(n,0,0,sb);
            System.out.println();
        }
    }

    static void Ans(int n, int left, int right, StringBuilder T) {
        if( left < right ){
            T.deleteCharAt(T.length()-1);
            return;
        }
        if( (left==right)&&(left==n)){
            System.out.println(T);
            T.deleteCharAt(T.length()-1);
            return;
        }
        
        if(left<n){
            T.append("(");
            Ans(n, left+1, right, T);
        }
        
        if(right<n){
            T.append(")");
            Ans(n, left, right+1, T);
        }
        
        if( !T.toString().equals("") ){
            T.deleteCharAt(T.length()-1);
        }
    }
    
}

/*
最近要開學了!  ( ~~~ 跟題目沒有什麼關係 ) >< 
請寫一個程式把所有合法括號匹配方式列出來!   
Ex. (())  ,  ((()())) , ()((()))  是合法的匹配方式 
      )( , (()))(  , ()(()(  是不合法的匹配方式
     合法匹配的括號 ， 從答案列的開頭到答案列某一點，左括弧次數永遠大於等於右括弧!  

    Ex. 合法匹配   ((()()))     
    字串 (        左括弧 : 1  >=   右括弧 : 0   	 
    字串 ((        左括弧 : 2  >=   右括弧 : 0   
    字串 (((        左括弧 : 3  >=   右括弧 : 0    
    字串 ((()        左括弧 : 3  >=   右括弧 : 1
    字串 ((()(        左括弧 : 4  >=   右括弧 : 1
    字串 ((()()        左括弧 : 4  >=   右括弧 : 2
    字串 ((()())        左括弧 : 4  >=   右括弧 : 3
    字串 ((()()))        左括弧 : 4  >=   右括弧 : 4        

    Ex. 不合法匹配    (()))(
   字串 (        左括弧 : 1  >=   右括弧 : 0 
   字串 ((        左括弧 : 2  >=   右括弧 : 0   
   字串 (()        左括弧 : 2  >=   右括弧 : 1
   字串 (())        左括弧 : 2  >=   右括弧 : 2
   字串 (()))        左括弧 : 2  <=   右括弧 : 3    
	!!! 右括弧次數大於左括弧了!  (()))( 為不合法匹配
--------------------------------------------------------------------------------
輸入說明
    輸入一個正整數 N ， 1 =< N <= 13 。
    N 代表有幾組括號要匹配
    Ex.
          N = 1 代表 一組括號 ()
          N = 2 代表有兩組括號  ()() 
範例輸入
    1
    2
    3
    4
--------------------------------------------------------------------------------
輸出說明
    輸出 N 組括號的所有合法匹配組合
    輸出方式請見範例
範例輸出
    ()

    (())
    ()()

    ((()))
    (()())
    (())()
    ()(())
    ()()()

    (((())))
    ((()()))
    ((())())
    ((()))()
    (()(()))
    (()()())
    (()())()
    (())(())
    (())()()
    ()((()))
    ()(()())
    ()(())()
    ()()(())
    ()()()()
*/