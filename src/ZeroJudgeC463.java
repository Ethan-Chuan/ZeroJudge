
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 *
 * @author Ethan
 */
public class ZeroJudgeC463 {
    
    public static void main(String[] args) {
        // DFS();
        ans();
    }
    
    static void showArrHeight(int[][] arrHeight){
        System.out.println("-----------------------");
        for(int i=1;i<arrHeight.length;i++){
            System.out.print(arrHeight[i][0] + " ");
            for(int j=1;j<arrHeight[i].length;j++){
                System.out.print(arrHeight[i][j] + " ");
            }
            System.out.println();
        }
    }
    
    /*
    ** #19
    ** Exception in thread "main" java.lang.StackOverflowError 
    **  at ZeroJudgeC463.DFSUtil(ZeroJudgeC463.java:62)
    */
    static void DFS(){
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] arrHeight = new int[n+1][];
        boolean[] visited = new boolean[n+1];
        int tmpInput, root = 0;
        
        for(int i=1;i<arrHeight.length;i++){
            tmpInput = scanner.nextInt()+1;
            arrHeight[i] = new int[tmpInput];

            arrHeight[i][0] = tmpInput-1;
            for(int j=1;j<tmpInput;j++){
                arrHeight[i][j] = scanner.nextInt();
                visited[arrHeight[i][j]] = true;
            }
        }
        //showArrHeight(arrHeight);
        
        // Find root
        for(int i=1;i<arrHeight.length;i++)
            if(!visited[i]){
                root = i;
                visited[i] = false;
                break;
            }
        System.out.println(root);
        
        // Recursive
        /*
        ** #19
        ** Exception in thread "main" java.lang.StackOverflowError 
        **  at ZeroJudgeC463.DFSUtil(ZeroJudgeC463.java:62)
        */
        DFSUtil(arrHeight, root);
        
        long sum = 0;
        for(int i=1;i<arrHeight.length;i++)
            sum += arrHeight[i][0];
        
         System.out.println(sum);
    }
    
    static int DFSUtil(int[][] arrHeight, int v){
        // Meet leaf.
        if(arrHeight[v].length == 1)
            return 0;
        
        int max=0, tmpMax=0;
        // Recur for all the vertices adjacent to this vertex.
        for(int i=1;i<arrHeight[v].length;i++){
            tmpMax = DFSUtil(arrHeight, arrHeight[v][i]);
            if(max < tmpMax)
                max = tmpMax;
        }
        
        arrHeight[v][0] = max+1;
        return arrHeight[v][0];
    }
    
    
    
    static void ans(){
        Scanner scanner = new Scanner(System.in);
        // p[i] = j, 表示 i 的雙親為 j 
        int[] p;
        // d[i] = height, 紀錄節點 i 的深度 height
        int[] d;
        // num[i] = count, 紀錄節點 i 的子節點個數count, 子節點計算過後減 1 
        int[] num;
        // 使用佇列儲存葉節點
        Queue<Integer> qLink = new LinkedList<>();
        // sum 總和各節點深度. 1+2+3+...+100000, 會超出int範圍
        long sum=0;
        // 節點個數 n
        int n = scanner.nextInt();
        
        p = new int[n+1];
        d = new int[n+1];
        num = new int[n+1];
        
        for(int i=1;i<num.length;i++){
            num[i] = scanner.nextInt();
            if(num[i]==0)
                // 葉節點(Leaf)
                qLink.offer(i);
            else
                for(int j=0;j<num[i];j++)
                    // 使用陣列 p 紀錄 scanner.nextInt() 的parent為 i 
                    p[scanner.nextInt()] = i;
        }
        
        int node = 0;
        while(!qLink.isEmpty()){
            // 取出葉節點
            node = qLink.poll();
            // 取出葉節點後，該節點的雙親節點個數減1
            num[p[node]] -= 1;
            // 取 該節點的雙親深度 與 自己深度+1 的較大值 存回該節點的雙親深度
            d[p[node]] = Math.max(d[p[node]], d[node]+1);
            // 當該節點的雙親的子節點個數為零時，代表所有的子節點都走訪過
            if(num[p[node]] == 0)
                // 加入該節點的雙親節點到佇列
                qLink.offer(p[node]);
        }
        
        for(int i=1;i<d.length;i++)
            sum += d[i];
        
        // 最後一個從 qLink 取出的就是 root 
        System.out.println(node);
        System.out.println(sum);
    }
}

/*
樹狀圖分析(Tree Analysis)
問題描述
本題是關於有根樹(rooted tree)。在一棵 n 個節點的有根樹中，每個節點都是以 1~n 的不同
數字來編號，描述一棵有根樹必須定義節點與節點之間的親子關係。一棵有根樹洽有一個節點沒有
父節點(parent)，此節點被稱為根節點(root)，除了根節點以外的每一個節點都洽有一個父節點，
而每個節點被稱為是它父節點的子節點(child)，有些節點沒有子節點，這些節點稱為葉節點(leaf)。
在當有根樹只有一個節點時，這個節點既是根節點同時也是葉節點。
在圖形表示上，我們將父節點畫在子節點之上，中間畫一條邊(edge)連結。例如，下圖中表示的
是一棵9個節點的有根樹，其中，節點1為節點6的父節點，而節點6為節點1的子節點；又5、3與
8都是2的子節點。節點4沒有父節點，所以節點4是根節點；而6、9、3與8都是葉節點。

|                           4
|                           |
|                      -----------
|                      |         |
|                      1         7
|                      |         |
|                      |         |
|                      6         2
|                                |
|                           -----------
|                           |    |    |
|                           5    3    8
|                           |
|                           |
|                           9

樹狀圖中的兩個節點u和v之間的距離d(u,v)定義為兩個節點之間邊的數量。如上圖中，
d(7,5)=2，而d(1,2)=3。對於樹狀圖中的節點v，我們以h(v)代表節點v的高度，其定
義是節點v和節點v下面最遠的葉節點之間的距離，而葉節點的高度定義為0。如上圖中，
節點6的高度為0，節點2的高度為2，而節點4的高度為4。此外，我們定義H(T)為T中所
有節點的高度總和，也就是說H(T) = sum(h(v)) for all v 。
給定一個樹狀圖T，請找出T的根節點以及高度總和H(T)。

--------------------------------------------------------------------------------
輸入說明
    第一行有一個正整數n代表樹狀圖的節點個數，節點的編號為1到n。
    接下來有n行，第i行的第一個數字k代表節點i有k個子節點，第i行接下來的k個數字就是這些子節點的編號。
    每一行的相鄰數字間以空白隔開。

範例輸入
範例一：
    7 
    0 
    2 6 7 
    2 1 4 
    0 
    2 3 2 
    0 
    0 

範例二：
    9 
    1 6 
    3 5 3 8 
    0 
    2 1 7 
    1 9 
    0 
    1 2 
    0 
    0 

--------------------------------------------------------------------------------
輸出說明
    輸出兩行各含一個整數，第一行是根節點的編號，第二行是H(T)。

範例輸出
範例一：
    5 
    4 

範例二：
    4 
    11 
*/