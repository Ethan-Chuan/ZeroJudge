
import java.util.Scanner;

/**
 *
 * @author Ethan
 */
public class ZeroJudgeB266 {
    static int row, column;
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        row = scanner.nextInt();
        column = scanner.nextInt();
        int M = scanner.nextInt();
        
        int[] B = new int[row*column];
        int[] op = new int[M];
        for(int i=0;i<B.length;i++)
            B[i] = scanner.nextInt();
        for(int i=0;i<M;i++)
            op[i] = scanner.nextInt();
        for(int i=M-1;i>=0;i--){
            switch(op[i]){
                case 0:
                    // rotate
                    B = rotateAnticlockwise90(B);
                    break;
                case 1:
                    // flip
                    B = flipUpDown(B);
                    break;
                default:
                    throw new RuntimeException();
            }
        }
        display(B);
    }
    
    static int[] flipUpDown(int[] intArr){
        int[] flipped = new int[intArr.length];
        
        for(int i=0;i<row;i++)
            for(int j=0;j<column;j++)
                flipped[i*column+j] = intArr[(row-i-1)*column+j];
            
        return flipped;
    }
    
    static int[] rotateClockwise90(int[] intArr){
        int[] rotated = new int[intArr.length];
        
        for(int i=0;i<row;i++)
            for(int j=0;j<column;j++)
                rotated[(row-i-1) + j*row] = intArr[i*column+j];
        
        swRC();
        return rotated;
    }
    
    static int[] rotateAnticlockwise90(int[] intArr){
        int[] rotated = new int[intArr.length];
        
        for(int i=0;i<row;i++)
            for(int j=column-1;j>=0;j--)
                rotated[i + (column-1-j)*row] = intArr[i*column+j];
        
        swRC();
        return rotated;
    }
    
    static void swRC(){
        int tmp = row;
        row = column;
        column = tmp;
    }
    
    static void display(int[] intArr){
        System.out.println(row + " " + column);
        StringBuilder sb = new StringBuilder(row*column*2);
        for(int i=0;i<row;i++){
            for(int j=0;j<column-1;j++)
                sb.append(intArr[i*column + j]).append(" ");
            sb.append(intArr[i*column + column - 1]).append("\n");
        }
        System.out.print(sb.toString());
    }
}
