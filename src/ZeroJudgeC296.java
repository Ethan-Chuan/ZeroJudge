
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author Ethan
 */
public class ZeroJudgeC296 {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int k = scanner.nextInt();
        int bomb = 0;
        
        // Java8
        List<Integer> arrayList = IntStream.rangeClosed(1, n).boxed().collect(Collectors.toCollection(ArrayList::new));
        //arrayList.forEach(item -> System.out.print(item + " "));
        //System.out.println();
        
        for(int i=0;i<k;i++){
            bomb = (bomb+(m-1)) % arrayList.size();
            // 刪除被炸的人
            arrayList.remove(bomb);
        }
        
        // 只剩下一個人
        if(arrayList.size() == 1)
            System.out.println(arrayList.get(0));
        // 最後一個人被炸，第一個就是幸運的人
        else if(bomb == arrayList.size())
            System.out.println(arrayList.get(0));
        else
            System.out.println(arrayList.get(bomb));
    }
    
}

/*
「定時 K彈」 是一個團康遊戲，N個人圍成一圈，由1號依序到N號，從1號開 始依序傳遞一枚
玩具炸彈，每次到第M個人就會爆炸，此人即淘汰，被淘汰的人要離開圓圈，然後炸彈再從該淘
汰者的下一個開始傳遞。遊戲之所以稱 K彈是因為這枚炸彈只會爆K次，在第 K次爆炸後，遊戲
即停止，而此時在第K個淘汰者的下一位遊戲者被稱為幸運者，通常就會要求表演節目。例如 
N=5，M=2，如果 K=2，炸彈會爆兩次，被爆炸淘汰的順序依是2與 4（參見下圖 ），這時5號
就是幸運者。如果K=3，剛才的遊戲會繼續，第三個淘汰是1號，所以幸運者是3號。 如果 K=4，
下一輪淘汰5號，所以3號是幸運者。
|
|       1
|    ↗    ↘
|  5          2
|   ↖      ↙
|     4 ← 3
|
|       1
|    ↗    ↘
|  5          3
|   ↖      ↙
|       4
|
|       1
|    ↗    ↘
|   5   ←   3
|
|
|      →
|   5      3
|      ←

--------------------------------------------------------------------------------
輸入說明
    輸入只有一行包含三個正整數，依序為N、M與 K，兩數中間有一個空格分開。其中 1 ≤ K<N。

範例輸入
    範例一 ：輸入
    5 2 4
    範例二：輸入
    8 3 6

--------------------------------------------------------------------------------
輸出說明
    請輸出幸運者的號碼，結尾有換行符號 。

範例輸出
    範例一：正確輸出
    3
    範例二：正確輸出
    4
*/