
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Ethan
 */
public class ZeroJudgeB966 {
    
    public static void main(String[] args) {
        ans();
    }
    
    static void ans(){
        Scanner scanner = new Scanner(System.in);
        List<Node> arrList;
        int n, s, e, sum;
        
        while(scanner.hasNextInt()){
            n = scanner.nextInt();
            arrList = new ArrayList<>();
            sum = 0;
            for(int i=0;i<n;i++)
                arrList.add(new Node(scanner.nextInt(), scanner.nextInt()));
            Collections.sort(arrList);
            //arrList.forEach(System.out::println);
            for(int i=0;i<n;i++){
                s = arrList.get(i).a;
                e = arrList.get(i).b;
                // s與e是否包含下一個區間的開始數值
                while( (i+1<n) && arrList.get(i+1).a < e){
                    // 包含下一個區間的全部，忽略此區間
                    if( arrList.get(i+1).b > e)
                        e = arrList.get(i+1).b;
                    i++;
                }
                sum = sum + e - s;
            }
            System.out.println(sum);
        }
    }
    
    static class Node implements Comparable<Node>{
        int a, b;
        
        Node(int a, int b){
            this.a = a;
            this.b = b;
        }

        @Override
        public int compareTo(Node o) {
            return this.a - o.a;
        }

        @Override
        public String toString() {
            return "Node{" + "a=" + a + ", b=" + b + '}';
        }
        
    }
    
    
    static void BruteForce(){
        boolean[] bol = new boolean[10000000];
        Scanner scanner = new Scanner(System.in);
        int n, left, right, dis, max=0, sum;
        
        while(scanner.hasNextInt()){ 
            n = scanner.nextInt();
            for(int i=0;i<=max;i++)
                bol[i] = false;
            max = 0;
            sum =0;
            for(int i=0;i<n;i++){
                left = scanner.nextInt();
                right = scanner.nextInt();
                max = max < right ? right : max;
                dis = right - left;
                for(int d=0;d<dis;d++)
                    bol[left+d] = true;
            }
            for(int i=0;i<=max;i++)
                if(bol[i]) sum++;
            System.out.println(sum);
            /*
            showMemory();
            if(n==10000000)
                break;
            */
        }
    }
    
    
    private static final double MEGABYTE = 1024L * 1024L;

    private static String bytesToMegabytes(long bytes) {
        return String.format("%.5f",bytes / MEGABYTE);
    }
    
    public static void showMemory(){
        // Get the Java runtime
        Runtime runtime = Runtime.getRuntime();
        // Run the garbage collector
        runtime.gc();
        // Calculate the used memory
        long memory = runtime.totalMemory() - runtime.freeMemory();
        System.out.println("Used memory is bytes: " + memory);
        System.out.println("Used memory is megabytes: "
                + bytesToMegabytes(memory));
    }
}

/*
給定一維座標上一些線段，求這些線段所覆蓋的長度，注意，重疊的部分只能算一次。
例如給定 4 個線段：(5, 6)、(1, 2)、(4, 8)、(7, 9)，如下圖，線段覆蓋長度為 6 。

|   0   1   2   3   4   5   6   7   8   9   10
|   ----------------------------------------------
|   ｜  ｜  ｜  ｜  ｜  ｜//｜  ｜  ｜  ｜  ｜
|   ----------------------------------------------
|   ｜  ｜//｜  ｜  ｜  ｜  ｜  ｜  ｜  ｜  ｜
|   ----------------------------------------------
|   ｜  ｜  ｜  ｜  ｜//｜//｜///｜//｜  ｜  ｜
|   ----------------------------------------------
|   ｜  ｜  ｜  ｜  ｜  ｜  ｜  ｜//｜//｜  ｜
|   ----------------------------------------------

--------------------------------------------------------------------------------
輸入說明
    輸入包含多筆測資。

    每筆側資中，
    第一列是一個正整數 N ，表示此測資有 N 個線段。
    接著的 N 列每一列是一個線段的開始端點座標整數值 L 和結束端點座標
    整數值 R ，開始端點座標值小於等於結束端點座標值，兩者之間以一個空格區隔。


    其中  30%的測資滿足， N < 100 ， 0 ≤ L , R < 1000 ，並且線段沒有重疊。
    其中  70%的測資滿足， N < 100 ， 0 ≤ L , R < 1000 ，並且線段可能重疊。
    其中100%的測資滿足， N < 10000 ， 0 ≤ L , R < 10000000 ，並且線段可能重疊。

範例輸入
    5
    160 180
    150 200
    280 300
    300 330
    190 210
    1
    120 120

--------------------------------------------------------------------------------
輸出說明
    每筆測資輸出一行，輸出其總覆蓋的長度。

    本題為嚴格比對，請務必按照說明進行輸出。

範例輸出
    110
    0
*/