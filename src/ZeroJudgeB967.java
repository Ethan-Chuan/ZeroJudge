
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;


/**
 *
 * @author Ethan
 */
public class ZeroJudgeB967 {
    // 紀錄計算中間過程最長長度
    static int md;
    // Adjacency List
    static LinkedList<Integer>[] adjLists;
    
    public static void main(String[] args) {
        ans();
    }
    
    
    static void ans(){
        Scanner scanner = new Scanner(System.in);
        int n, a, b, curLen=0;
        // p[i] = j, 表示 i 的雙親為 j 
        int[] p = new int[100000+1];
        // maxD[i][0] = Farthest kinship, 紀錄節點i的子樹的最遠血緣距離
        // maxD[i][1] 最長深度 ； maxD[i][2] 第二長深度
        int[][] maxD = new int[100000+1][3];
        // num[i] = count, 紀錄節點i的子節點個數count, 子節點計算過後減1 
        int[] num = new int[100000+1];
        // 儲存是否為雙親
        boolean[] isParent = new boolean[100000+1];
        // 使用佇列儲存葉節點
        Queue<Integer> qLink = new LinkedList<>();
        
        while(scanner.hasNextInt()){
            // 成員個數 n
            n = scanner.nextInt();
            
            // 初始化
            maxD[0][0] = -1;
            maxD[0][1] = -1;
            for(int i=1;i<curLen;i++){
                p[i] = 0;
                num[i] = 0;
                isParent[i] = false;
                maxD[i][0] = 0;
                maxD[i][1] = 0;
                maxD[i][2] = 0;
            }
            
            for(int i=1;i<n;i++){
                a = scanner.nextInt()+1;
                b = scanner.nextInt()+1;
                p[b] = a;
                num[a]++;
                // a是雙親，設定isParent為true
                isParent[a] = true;
            }
            
            // 修正長度以符合目前的陣列長度
            curLen = n + 1;
            
            // 找尋葉節點
            for(int i=1;i<curLen;i++)
                if(!isParent[i]) qLink.offer(i);// 只要不是雙親，就是葉節點
           

            int node = 0, result=0, max=0;
            while(!qLink.isEmpty()){
                // 取出葉節點
                node = qLink.poll();    
                // 取出葉節點後，該節點的雙親節點個數減1
                num[p[node]] -= 1;
                
                // 計算血緣距離
                result = maxD[node][1] + 1;
                if(maxD[p[node]][1] == 0){ //走訪第一個小孩
                   maxD[p[node]][1] = result;
                }else if(maxD[p[node]][2] == 0){ //走訪第二個小孩
                    if(maxD[p[node]][1]>=result){
                        maxD[p[node]][2] = result;
                    }else{
                        maxD[p[node]][2] = maxD[p[node]][1];
                        maxD[p[node]][1] = result;
                    }
                }else{ //走訪第三個小孩以後
                    if(result >= maxD[p[node]][1]){
                        maxD[p[node]][2] = maxD[p[node]][1];
                        maxD[p[node]][1] = result;
                    }else if(result > maxD[p[node]][2]){
                        maxD[p[node]][2] = result;
                    }
                }
                maxD[p[node]][0] = maxD[p[node]][1] + maxD[p[node]][2];
                if(max<maxD[p[node]][0]) max = maxD[p[node]][0];
                
                // 當該節點的雙親的子節點個數為零時，代表所有的子節點都走訪過
                if(num[p[node]] == 0)
                    // 加入該節點的雙親節點到佇列
                    qLink.offer(p[node]);
            }
            
            System.out.println(max);
        }
    }
    
    
    static void DFS(){
        Scanner scanner = new Scanner(System.in);
        int a, b, root = -1, rd, n;
        boolean[] isChild;
        
        while(scanner.hasNextInt()){
            n = scanner.nextInt();
            md = 0;
            adjLists = new LinkedList[n];
            isChild = new boolean[n];
            for(int i=0;i<n;i++)
                adjLists[i] = new LinkedList<>();
            
            // 建立由雙親到小孩的有向圖
            for(int i=1;i<n;i++){
                a = scanner.nextInt();
                b = scanner.nextInt();
                adjLists[a].offer(b);
                // b是小孩，設定isChild為true
                isChild[b] = true;
            }
            
            // 找出root
            for(int i=0;i<n;i++){
                if(!isChild[i]){ // 只要不是小孩，就是root
                    root = i;
                    break;
                }
            }
            
            // rd為從root出發最長長度，函式DFS過程會計算中間過程最長長度md
            rd = DFSUtil(root);
            // md為中間過程最長長度，rd與md取最大
            md = Math.max(rd, md);
            
            System.out.println(md);
        }
    }
    
    static int DFSUtil(int node){
        // max1與max2保留最大前兩個的深度
        int max1=0, max2=0, result;
        
        if(adjLists[node].isEmpty()){ // 沒有小孩，遞迴中止
            return 0;
        }
        if(adjLists[node].size() == 1){ // 小孩只有一個
            return DFSUtil(adjLists[node].get(0))+1;
        }else{ // 小孩超過兩個
            // 走訪每一個小孩，找出最長深度的前兩名，最長深度儲存到max1，第二長深度儲存到max2
            for(int i=0;i<adjLists[node].size();i++){
                // 該小孩的深度
                result = DFSUtil(adjLists[node].get(i)) + 1;
                switch (i) {
                    case 0:
                        // 走訪第一個小孩時
                        max1 = result;
                        break;
                    case 1:
                        // 走訪第二個小孩時
                        if(max1>=result){
                            max2 = result;
                        }else{
                            max2 = max1;
                            max1 = result;
                        }break;
                    default:
                        // 走訪第三個小孩以後
                        if(max1 <= result){
                            max2 = max1;
                            max1 = result;
                        }else if(max2 < result){
                            max2 = result;
                        }break;
                }
            }
            // 中間的節點分支度大於等於2，最大血緣關係為小孩中最長深度與第二長深度相加
            md = Math.max(md, max1+max2);
            return max1;
        }
    }
    
}
/*
小宇有一個大家族。有一天，他發現記錄整個家族成員和成員間血緣關係的家族族譜。
小宇對於最遠的血緣關係 (我們稱之為"血緣距離") 有多遠感到很好奇。

下圖為家族的關係圖。
 0 是 7 的孩子， 1、2 和 3 是 0 的孩子， 4 和 5 是 1 的孩子， 6 是 3 的孩子。
我們可以輕易的發現最遠的親戚關係為 4(或 5) 和 6 ，他們的"血緣距離"是 4 (4→1→0→3→6)。

|                           7
|                          /
|                         /
|                        0
|                     /  |  \
|                    /   |   \
|                   1    2    3
|                 /  \         \  
|                /    \         \
|               4     5          6
|

給予任一家族的關係圖，請找出最遠的"血緣距離"。
你可以假設只有一個人是整個家族成員的祖先，而且沒有兩個成員有同樣的小孩。

--------------------------------------------------------------------------------
輸入說明
    輸入包含多筆測資。

    每筆側資中，
    第一行為一個正整數 N 代表成員的個數，每人以 0~N-1 之間唯一的編號代表。
    接著的 N-1 行，每行有兩個以一個空白隔開的整數 a 與 b (0 ≤ a, b ≤ N-1)，代表 b 是 a 的孩子。

    其中  10%的測資滿足， 2 ≤ N ≤ 100 ，整個家族的祖先最多 2 個小孩，其他成員最多一個小孩。
    其中  40%的測資滿足， 2 ≤ N ≤ 100。
    其中  70%的測資滿足， 2 ≤ N ≤ 2000。
    其中100%的測資滿足， 2 ≤ N ≤ 100000。

範例輸入
8
0 1
0 2
0 3
7 0
1 4
1 5
3 6
4
0 1
0 2
2 3

--------------------------------------------------------------------------------
輸出說明
    每筆測資輸出一行，輸出最遠"血緣距離"的答案。

    本題為嚴格比對，請務必按照說明進行輸出。

範例輸出
4
3

--------------------------------------------------------------------------------
ans()方法計算到祖先(root)時，要考慮陣列0的位置會被再次計算

(1)只有一條族譜
3
0 2
1 0

(2)兩條族譜，這兩條計算到祖先(root)時，最後陣列0會再次計算，
所以如果0是祖先的孩子 且 0的這條族譜比另一條大時，就會出錯。
8
0 2
5 6
6 7
3 4
1 0
2 3
1 5
*/