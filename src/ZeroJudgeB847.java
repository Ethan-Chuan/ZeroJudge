
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;

/**
 *
 * @author Ethan
 */
public class ZeroJudgeB847 {
    
    public static void main(String[] args) {
        // Using BufferedReader to reduce input operation time.
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
            String lineStr;
            char chr;
            BigDecimal[] ascii=new BigDecimal[26], rate=new BigDecimal[26];
            BigDecimal letterLen;
            StringBuilder sb1, sb2;
            
            while( (lineStr=br.readLine()) != null ){
                // Initialize
                for(int i=0;i<26;i++){
                    ascii[i] = BigDecimal.ZERO;
                    rate[i] = new BigDecimal("100");
                }
                letterLen = new BigDecimal(lineStr.length());
                
                // Adjust number of letter.
                for(int i=0;i<lineStr.length();i++){
                    chr = lineStr.charAt(i);
                    if('a'<=chr && chr<='z'){
                        ascii[chr-'a'] = ascii[chr-'a'].add(BigDecimal.ONE);
                    }else if('A'<=chr && chr<='Z'){
                        ascii[chr-'A'] = ascii[chr-'A'].add(BigDecimal.ONE);
                    }else{
                        letterLen = letterLen.subtract(BigDecimal.ONE);
                    }
                }
                
                // Using BigDecimal.ROUND_HALF_EVEN to round off 2 decimal places.
                // Using StringBuilder to reduce output operation times.
                sb1=new StringBuilder(156);
                sb2=new StringBuilder(156);
                for(int i=0;i<26;i++){
                    rate[i] = rate[i].multiply( ascii[i].divide(letterLen, 4, BigDecimal.ROUND_HALF_EVEN) );
                    sb1.append( String.format("%.0f ", ascii[i]) );
                    sb2.append( String.format("%.2f ", rate[i]) );
                }
                sb1.append("\n");
                sb2.append("\n");
                System.out.print(sb1);
                System.out.print(sb2);
            }
        } catch (IOException ex) {}
    }
    
}
