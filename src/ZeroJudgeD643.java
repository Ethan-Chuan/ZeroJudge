
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 *
 * @author Ethan
 */
public class ZeroJudgeD643 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String incantation = scanner.nextLine();
        int[] factors = findFactor(incantation.length());
        result(incantation, factors);
    }
    
    // Find all factors of n.
    static int [] findFactor(int n) {
        int factorNum=0, maxLen = (int)Math.sqrt(n);
        int[] factor = new int[maxLen*2];
        
        for(int i=1;i<=maxLen;i++){
            if(n%i==0){
                factor[factorNum++] = i;
                if((n/i)!=i)
                    factor[factorNum++] = n/i;
            }
        }
        Arrays.sort(factor, 0, factorNum);
        return Arrays.copyOfRange(factor, 0, factorNum);
    }
    
    static void result(String incantation, int[] factors){
        boolean hasNew = false;
        int len = factors.length - 1;
        PriorityQueue<String> pQueue = new PriorityQueue<>(incantation.length());
        
        for(int i=0;i<len;i++){
            // Get current factor.
            int factor = factors[i];
            
            // Add substring of incantation to priority queue. 
            for(int j=0;j<incantation.length()/factor;j++)
                    pQueue.offer(incantation.substring(j*factor, (j+1)*factor));
            
            // Concatenate elements in priority queue.
            StringBuilder sb = new StringBuilder();
            while(!pQueue.isEmpty())
                sb.append(pQueue.poll());
            
            // If the string is not the same with original,
            // output the string and change flag.
            if(!sb.toString().equals(incantation)){
                System.out.println(sb.toString());
                hasNew = true;
            }
        }
        
        // If the flag is false, then output "bomb!".
        if(!hasNew)
            System.out.println("bomb!");
        
    }
}

/*
    繼上次你解決了梅蘭城符咒室的災難之後，
    現在法師們又有新的問題了。
    「想新符咒真是麻煩~__~」
    於是法師們想出一個絕妙的辦法來產生新符咒
    叫做「勞動的符咒」
    假設有一長度為8的符咒bcadefpa，
    取數字2表示兩個字一組分成bc,ad,ef,pa
    然後將這四組字按照ASCII的順序排列成為adbcefpa
    這樣就可以製造出新符咒了
    雖然省去了想新符咒的麻煩，
    但是作這樣的分解排列卻讓法師們陷入另外一場混亂中，
    你可以寫個程式幫幫他們嗎？
--------------------------------------------------------------------------------
輸入說明
    每個測資點的測資僅一列。即原符咒內容。
    (字元長度不超過100000個字元，僅包含小寫英文字母)
    假設符咒長度是12個字元，
    那麼你必須由小到大列出1、2、3、4、6個字元一組的所有符咒
    (也就是12的因數。當然了不必列12，因為和原符咒一樣)
    萬一發生分解排列後符咒與原本相同的話，
    那麼就不用輸出該符咒。
範例輸入
    efpabcad
--------------------------------------------------------------------------------
輸出說明
    輸出數種不同類型的符咒。一條符咒一列。
    萬一無法產生新的符咒，
    請輸出bomb!
範例輸出
    aabcdefp
    adbcefpa
    bcadefpa
*/