
import java.util.Scanner;

/**
 *
 * @author Ethan
 */
public class ZeroJudgeC292 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // 控制左、上、右、下
        final int R[] = {0, -1, 0, 1};
        final int C[] = {-1, 0, 1, 0};
        int n, x, curC, curR, currentCount;
        int[][] arr;
        StringBuilder sb;

        n = scanner.nextInt();
        arr = new int[n][n];
        // 第1個數字的位置A[MidC][MidR];
        curC = (n - 1) / 2;
        curR = (n - 1) / 2;
        // 建立足夠容量的StringBuilder實例
        sb = new StringBuilder(n * n + 1);

        x = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }

        // 加入第1個數字
        sb.append(arr[curR][curC]);

        currentCount = 0;
        for (int i = 0; i < n - 1; i++) {
            currentCount++;
            // 兩個一組
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < currentCount; k++) {
                    sb.append(arr[curR += R[x]][curC += C[x]]);
                }
                x = (x + 1) % 4;
            }
        }

        for (int i = 0; i < n - 1; i++) {
            sb.append(arr[curR += R[x]][curC += C[x]]);
        }

        System.out.println(sb);
    }

}
