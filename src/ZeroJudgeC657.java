
import java.util.Scanner;

/**
 *
 * @author Ethan
 */
public class ZeroJudgeC657 {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str;
        char tmpChar, ansChar = '0';
        int tmpLen, ansLen;
        
        while(scanner.hasNextLine()){
            str = scanner.next();
            ansLen = 0;
            int j;
            for(int i=0; i<str.length(); i++){
                tmpChar = str.charAt(i);
                tmpLen = 1;
                for(j=i+1; j<str.length(); j++){
                    if(tmpChar==str.charAt(j)){
                        tmpLen++;
                    }else{
                        i = j - 1;
                        break;
                    }
                }
                //System.out.println(i + " " + tmpLen + " " + tmpChar);
                if(tmpLen>ansLen){
                    ansLen = tmpLen;
                    ansChar = tmpChar;
                }
                if(j==str.length()) break;
            }
            System.out.println(ansChar + " " + ansLen);
        }
    }
    
}
