
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

/**
 *
 * @author Ethan
 */
public class ZeroJudgeC471 {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int n;
        long result, sum;
        
        
        n = scanner.nextInt();
        result = 0;
        sum = 0;
        Box[] b = new Box[n];
        for(int i=0;i<b.length;i++){
            b[i] = new Box();
        }
        for(int i=0;i<n;i++){
            b[i].w = scanner.nextInt();
        }
        for(int i=0;i<n;i++){
            b[i].f = scanner.nextInt();
        }
        Arrays.sort(b, new BoxComparator());
        /*
        for(int i=0;i<n;i++){
            System.out.println(i + " : w=" + b[i].w + ", f=" + b[i].f);
        }*/
        // 最小消耗能量
        for(int i=0;i<n-1;i++){
            sum += b[i].w;
            result += sum*b[i+1].f;
        }
        System.out.println(result);
    }
    
}

class Box{
    int w;
    int f;
}

// o1.w*o2.f 與 o2.w*o1.f 大的放在下層
class BoxComparator implements Comparator<Box>{

    @Override
    public int compare(Box o1, Box o2) {
        return o1.w*o2.f - o2.w*o1.f;
    }

}