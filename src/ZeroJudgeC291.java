
import java.io.DataInputStream;
import java.io.IOException;

/**
 *
 * @author Ethan
 */
public class ZeroJudgeC291 {
    
    public static void main(String[] args) throws IOException {
        Reader reader = new Reader();
        int n = reader.nextInt();
        int[] arr = new int[n];
        int i, grp;
        for(i=0;i<n;i++){
            arr[i] = reader.nextInt();
        }
        
        grp=0;
        i=0;
        while(n>0){ 
            if(arr[i]>=0){
                if((arr[i])==i){
                    arr[i] = -1;
                    i = 0;
                    grp += 1;
                    n--;
                }else{
                    int tmp = i;
                    i = arr[i];
                    arr[tmp] = -1;
                    n--;
                    if(arr[ arr[i] ] == -1){
                        arr[i] = -1;
                        i = 0;
                        grp += 1;
                        n--;
                    }
                }
            }
            if(arr[i]==-1)
                i++;
        }
        System.out.println(grp);
    }
    
    
    /*
    * Fastest way to read data.
    * The max number of edges is 9999999, so it needs
    * first line and 999999999 line, and every lines 4 bytes.
    * So it at least needs 40 millions bytes buffer.
    */
    static class Reader {

        private final int BUFFER_SIZE = (1 << 25) + (1 << 24);
        private final DataInputStream din;
        private final byte[] buffer;
        private int bufferPointer, bytesRead;

        public Reader() {
            din = new DataInputStream(System.in);
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }

        public int nextInt() throws IOException {
            if (this.bytesRead == -1) {
                return -1;
            }

            int ret = 0;
            byte c = read();
            while (c < '0' || '9' < c) {
                c = read();
            }

            do {
                ret = ret * 10 + (c - '0');
            } while ((c = read()) >= '0' && c <= '9');

            return ret;
        }

        private void fillBuffer() throws IOException {
            // If the end of stream has been reached, it return -1.
            bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
            if (bytesRead == -1) {
                buffer[0] = -1;
            }
        }

        private byte read() throws IOException {
            if (bufferPointer == bytesRead) {
                fillBuffer();
            }
            byte c = buffer[bufferPointer++];
            return c;
        }

        public void close() throws IOException {
            if (din == null) {
                return;
            }
            din.close();
        }
    }
}
