
import java.io.DataInputStream;
import java.io.IOException;

/**
 *
 * @author Ethan
 */
public class ZeroJudgeD575 {
    
    public static void main(String[] args) {
        long x1,y1,x2,y2,d,diff;
        Reader reader = new Reader();
        
        try {
            do{
                x1 = reader.nextLone();
                y1 = reader.nextLone();
                x2 = reader.nextLone();
                y2 = reader.nextLone();
                d = reader.nextLone();
                if(d<1)
                    break;
                diff = Math.abs(x1-x2) + Math.abs(y1-y2);
                if(diff>d){
                    System.out.println("alive");
                }else{
                    System.out.println("die");
                }
            }while(true);
        } catch (IOException ex) {
        }finally{
            try {
                reader.close();
            } catch (IOException ex) {}
        }
    }
    
    static class Reader{
        private final int BUFFER_SIZE = 10240;
        private DataInputStream din;
        private byte[] buffer;
        private int bufferPointer, bytesRead;
        
        Reader(){
            din = new DataInputStream(System.in);
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = 0;
            bytesRead = 0;
        }
        
        private void fillBuffer() throws IOException{
            bytesRead = din.read(buffer, bufferPointer=0, BUFFER_SIZE);
            if(bytesRead==-1){
                buffer[0] = -1;
            }
        }
        
        private byte read() throws IOException{
            if(bytesRead == bufferPointer){
                fillBuffer();
            }
            return buffer[bufferPointer++];
        }
        
        public void close() throws IOException{
            if(din == null){
                return;
            }
            din.close();
        }
        
        public long nextLone() throws IOException{
            long ret = 0;
            
            byte c = read();
            if(bytesRead==-1){
                return -1;
            }
            while(c <= ' '){
                c = read();
                if(bytesRead==-1){
                    return -1;
                }
            }
            
            boolean neg = (c == '-');
            if(neg){
                c = read();
                if(bytesRead==-1){
                    return -1;
                }
            }
            
            do{
                ret = ret*10 + (c - '0');
            }while( (c=read()) >= '0' && c <= '9');
            
            if(neg){
                return -ret;
            }else{
                return ret;
            }
        }
    }
}

/*
傳說在「巴拉巴拉巴拉」(王國名稱)王國曾經有一個被屠殺的小鎮叫做「巴拉巴拉巴拉」(小鎮名稱)
後來「巴拉巴拉巴拉」王國有一群「巴拉巴拉巴拉」(也許是科學家或魔法師之類的)
他們研究發現，「巴拉巴拉巴拉」小鎮被屠村原來是因為遭到了天譴。
特別的是，末日審判的範圍是十字型擴張的。

在方格座標上(假設最中央的格子是0,0，右0,1，上-1,0，左0,-1，下1,0)，
假如範圍是1，中心座標是0,0，那麼末日審判的範圍即是下圖的●部分…
○○○○○
○○●○○
○●●●○
○○●○○
○○○○○

範圍2
○○●○○
○●●●○
●●●●●
○●●●○
○○●○○

範圍3
○○○●○○○
○○●●●○○
○●●●●●○
●●●●●●●
○●●●●●○
○○●●●○○
○○○●○○○

現在給定審判的中心座標、以及「巴拉巴拉巴拉」王國首都「巴拉巴拉巴拉」(城市名稱)的位置、審判範圍，
請你判斷首都會不會遭受天遣而滅亡。
--------------------------------------------------------------------------------
輸入說明：
    共計10個測資點。

    每個測資點有多組測試資料。
    每組測資一行。
    第一和第二個數字是天遣的中心座標，
    第三和第四個數字是王國首都的座標，(所有座標值保證介於-2147483648~2147483647之間)
    第五個數字是天遣的有效範圍r(1<=r<=2147483647)

範例輸入：
    0 0 1 1 1
    0 0 1 1 2
    0 0 -1 2 3
    -1 0 1 2 3
    0 0 50 50 100
    0 0 50 50 99
--------------------------------------------------------------------------------
輸出說明：
    如果首都座標在天遣的範圍內而招致滅亡，請輸出die
    如果首都座標不在天遣範圍而逃過一劫，請輸出alive

範例輸出：
    alive
    die
    die
    alive
    die
    alive
*/